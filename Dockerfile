FROM alpine:latest
MAINTAINER Tru Huynh <tru@pasteur.fr>

RUN apk update && apk upgrade
RUN apk add git
RUN date +"%Y-%m-%d-%H%M" > /last_update
